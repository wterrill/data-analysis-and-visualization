#options(expressions=50000)
source('~/git/Data Analysis and Visualization/1st homework/logfactorialloop.R')
source('~/git/Data Analysis and Visualization/1st homework/logFactorialRecursive.R')


#DF <- data.frame()
#cat("n loop recursion system \n")
#loop2 = vector("list", 0)
#recursion2 = vector("list", 0)
#system2 = vector("list", 0)


loop2 = c()
recursion2 = c()
system2 = c()
x = c()
x2 = c()


i=1
val = 3


for (num in seq(1, 2319, by = 1))
{
  loop = system.time(logfactorialloop(num))
  if (num<2319)
    {
      recursion = system.time(logFactorialRecursive(num))
    }
  system = system.time(lfactorial(num))
  
  
  #print(loop)
  #print(recursion)
  #print(system)
  
  
  print(num)
  
  
  #cat("--------- value for x = ",num," ---------")
  #newrow = c(num, loop[1])#, recursion[1], system[1])
  
  
  loop2[i] = loop[val]
  if (num<2319)
  {
    recursion2[i] = recursion[val]
    x2[i] = num
  }
  system2[i] = system[val]
  x[i] = num
  i=i+1
  #DF = rbind(DF,newrow)
}
#View(DF)



#logfact_vec <- function(n) sum(log(1:n))




g_range <- range(0, recursion2, loop2)
plot(x,loop2,type="o", col="blue", ylim=g_range, axes=FALSE, ann=FALSE)
lines(x2, recursion2, type="o", pch=22, lty=2, col="red")
lines(x,system2, type="o", pch=22, lty=2, col="green")

axis(1, las=1, at=seq(0,num,num/10))
axis(2, las=1, at=seq(0, g_range[2], g_range[2]/10))


title(main="Comparison of recursion, looping and system for log factorial function", col.main="red", font.main=4)
title(ylab="Elapsed Time", col.lab=rgb(1,0,0),font.main=4)
title(xlab="Value input into log factorial", col.lab=rgb(1,0,0),font.main=4)
box()
#legend(1, g_range[2], c("loop2","recursion2", "system2"),
#       col=c("blue","red", "green"),bty="n") 

#legend("topleft", names(c("loop2","recursion2", "system2")), cex=0.8, col=c("blue","red", "green"), 
#       lty=1:3, lwd=2, bty="n");

legend(1, g_range[2], c("loop method","recursion method", "system function"), cex=0.8, 
       col=c("blue","red", "green"), pch=21:22, lty=1:2, bty="n");





