---
title: "HW2"
author: "William L Terrill"
date: "September 10, 2016"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Homework #2 - Visualizations

##1. **Using the mpg data, describe the relationship between highway mpg and
car manufacturer. Describe which companies produce the most and least
fuel efficient cars, and display a graph supporting your conclusion.**


To investigate this phenomenon in the mpg dataset, I took the hwy mpg data and arranged it by the median value to produce the graph below.  Here is the code and subsequent box and whisker plot.
```{r cars}
library(ggplot2)
print(ggplot(mpg, aes(reorder(manufacturer, -hwy, median), hwy)) +
       geom_boxplot() +
       coord_flip() +
       scale_x_discrete("manufacturer"))
```

Interpreting this graph, land rover has the lowest average highway mpg with a strong cluster of data near the median since no whiskers are seen.  It should be noted that both dodge and jeep produce cars with lower mpg than land rover indicated by the 1st quartile range that extends below land rovers lower value, however both companies also produce cars with high mpg that bring up the median value above land rover's.  

On the opposite end of the spectrum, honda has the highest median value of hwy mpg, which in highly oriented to the lower end of the interquartile range. However, Volkswagen has two notable outliers that are significantly above the highest values of any other cars.    
  
    

##2 **Using the mpg data, explore the three-way relationship between highway
mpg, city mpg, and model class. What are your observations? Display a
graph supporting these observations.**

In order to create this dataset, I had to do things a little bit more manually in order to arrange the graph in the method that I desired.  So, for each car class category, I computed the median mpg as show below, repeating for all seven of the different classes (suv, minivan, etc) in order to be able to rank them by class in the overall graph.  
```{r}
median(mpg[ which(mpg$class=='pickup'), ]$hwy)
```

Using these values, I created a new column "type" that had the same values as "class", but I manually created a factor that gave a set order to the various classes of cars so that they could be ordered in the final graph.

```{r}
cars<-mpg
cars$type = factor(cars$class, levels = c('pickup', 'suv', 'minivan', '2seater' ,'subcompact' , 'midsize','compact'))
```

With the factor created, it was straightforward to graph hwy versus cty values and arrange it by median values as shown below.
```{r}
print(ggplot(cars, aes(x=hwy, y=cty)) +
          geom_point() +
          facet_grid(type~.))
```
 
I expected that SUV's were going to have the lowest cty/hwy mpg of the group simply because of the constant bad press about their capabilities.  However, although SUV's were low, pickup trucks were lower with a median value of 17mpg, while SUV's have a median value of 17.5.  Additionally, the graph shows that the both pickup trucks and SUV's had values for the lowest mpg, but SUV's had a model that was 6 times the value of the lowest mpg. This model even approached the median value of the highest category.

On the upper end of the spectrum, the median values were more tightly clustered, with both compact and midsize having a median highway mpg of 27mpg. I had anticipated that the subcompact class would be rated higher than the compact class, and at the higher end, there are models in the subcompact class that equal the compact class in terms of both city and highway mpg.  However, the median is pulled down in the subcompact class by the rather large range of values, and there are a number of subcompact cars that have mpg lower than minivans.




##3 **What are the pros and cons of using a histogram vs a box plot? Which one
will you prefer for what purpose?**

Box plots show specific aspects of the data succintly.  It shows the median value clearly and emphasized the quartiles of the data and any outliers.  However, it is lossy for nuances in the data since any multimodal distribution information can never be shown on a boxplot.  On the other hand, histograms are more difficult to extract information about the median and quartile values due to the more continuous range of values.

Therefore, boxplots are more useful for situations that rapidly summarize data in a 'bare bones' fashion where fast comparisons are needed, while histograms are more useful for in-depth study of the nuances of a given dataset.  For example, for the boxplot in problem #1 above, the below is the boxplot just for the manufacturer "dodge"

```{r}
cars <- mpg[(mpg$manufacturer == "dodge"),]
print(ggplot(cars, aes(manufacturer, hwy)) +
        geom_boxplot() +
        coord_flip() +
        scale_x_discrete("manufacturer"))
```

The box plot was useful in question #1 because of the rapid comparison with other manufacturers.  However, in this case, the boxplot itself tells very little.  On the other hand, the histogram for the same data is shown below

```{r}
print(ggplot(data=cars, aes(hwy)) + 
        geom_histogram(binwidth=2,fill="white",colour="black")+
        labs(title="Histogram for highway mpg for Dodge") +
        labs(x="Highway MPG", y="Count"))
```
        
In this case, the high number of values for 16 mpg is clearly seen as is the low number of 20 mpg values. Both are  completely obscured in the boxplot. 

It should be noted, though, that the discussion above for the histogram is highly dependent on the binwidth size chosen. Too high of a binwidth could obscure nuances in the data more effectively than a boxplot.



##4 **Generate two sets of N random points using the function runif and display
a corresponding scatter plot. If you save the file to disk, what is the
resulting file size for the following file formats: ps, pdf, jpeg, png? How do
these values scale with increasing N?**

The code shown below runs through various values of N up until N=250000, and saves that number of random points on a scatterplot to disk, and then creates a datafram of those values in order to create the graph below.
```{r, eval=FALSE}
# Initialize data 
count = 10000
end = 250000
beginning = 0
# Initialize Data Structures
N<-vector(mode = "numeric", length = (end-beginning)/count)
sizeps<-vector(mode = "numeric", length = (end-beginning)/count)
sizepdf<-vector(mode = "numeric", length = (end-beginning)/count)
sizepng<-vector(mode = "numeric", length = (end-beginning)/count)
sizejpg<-vector(mode = "numeric", length = (end-beginning)/count)
names(size)<-"size"
names(en)<-"N"
i=0
for (j in seq(beginning, end, by=count)) #loop through N values
{
  i=i+1
  x = runif(j)  #create random data
  y = runif(j)
  both = data.frame(x,y) #put it in a dataframe
  ggplot(both,aes(x=both$x,y=both$y)) + geom_point() #graph it...
  ggsave(file = "myPlot.ps") # save it to disk with four different filetypes.
  ggsave(file = "myPlot.pdf")
  ggsave(file = "myPlot.png")
  ggsave(file = "myPlot.jpg")
  sizeps[i] <- file.info("myPlot.ps")$size   #collect the filesize of the resulting file.
  sizepdf[i] <- file.info("myPlot.pdf")$size
  sizepng[i] <- file.info("myPlot.png")$size
  sizejpg[i] <- file.info("myPlot.jpg")$size
  N[i] <- j     #save the N value that the file sizes were created with.
}
final <- data.frame(N,sizeps,sizepdf,sizepng,sizejpg)  #create dataframe with the above information
plot<-ggplot(final, aes(N)) +     # graph it.
  geom_line(aes(y = sizeps, colour = "size PS")) +
  geom_line(aes(y = sizepdf, colour = "size PDF")) +
  geom_line(aes(y = sizepng, colour = "size PNG")) +
  geom_line(aes(y = sizejpg, colour = "size JPG"))
print(plot)
```

![Plot of PS, PDF, PNG and JPG file size versus N](./final plot-250000.jpg)

The results are interesting.  The postscript (PS) file and the PDF file, both rose linearly with the size of N, but with PDF rising at a slower pace than PS.  PNG and JPG, on the other hand, had a much larger filesize up until around N=25000, where the filesize begain to fall since more and more of the graph was colored uniformly (in this case, black), and therefore the compression algorithms could start to take effect. As N continues to increase, the values for filesize continue to decrease until they are about the original size with N=0 (an empty set of axes.) 


