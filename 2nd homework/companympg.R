library(ggplot2)



# print(ggplot(mpg, aes(reorder(manufacturer, -hwy, median), hwy)) +
#        geom_boxplot() +
#        coord_flip() +
#        scale_x_discrete("manufacturer"))

# plot9<-qplot(x = hwy,
#              y = cty,
#              data = mpg,
#              facets = class~.,
#              main = "MPG vs. weight by transmission and engine")
# print(plot9)

# median(mpg[ which(mpg$class=='pickup'), ]$hwy)
# cars<-mpg
# cars$type = factor(cars$class, levels = c('pickup', 'suv', 'minivan', '2seater' ,'subcompact' , 'midsize','compact'))
# print(ggplot(cars, aes(x=hwy, y=cty)) +
#           geom_point() +
#           facet_grid(type~.))

#Box plot does not display the multimodal nature of the distribution that the histogram shows.  
#However, it is easier to ready the median and Inter quartile range.
#box plots more lossy. emphasize quartiles and outliers
#box plots reveals info
# count = 10000
# end = 250000
# beginning = 0
# N<-vector(mode = "numeric", length = (end-beginning)/count)
# sizeps<-vector(mode = "numeric", length = (end-beginning)/count)
# sizepdf<-vector(mode = "numeric", length = (end-beginning)/count)
# sizepng<-vector(mode = "numeric", length = (end-beginning)/count)
# sizejpg<-vector(mode = "numeric", length = (end-beginning)/count)
# names(size)<-"size"
# names(en)<-"N"
# i=0
# for (j in seq(beginning, end, by=count))
# {
#   i=i+1
#   x = runif(j)
#   y = runif(j)
#   both = data.frame(x,y)
#   ggplot(both,aes(x=both$x,y=both$y)) + geom_point()
#   ggsave(file = "myPlot.ps")
#   ggsave(file = "myPlot.pdf")
#   ggsave(file = "myPlot.png")
#   ggsave(file = "myPlot.jpg")
#   sizeps[i] <- file.info("myPlot.ps")$size
#   sizepdf[i] <- file.info("myPlot.pdf")$size
#   sizepng[i] <- file.info("myPlot.png")$size
#   sizejpg[i] <- file.info("myPlot.jpg")$size
#   N[i] <- j
# }
# final <- data.frame(N,sizeps,sizepdf,sizepng,sizejpg)
# #plot<-ggplot(final,aes(N,size))+geom_line()
# plot<-ggplot(final, aes(N)) + 
#   geom_line(aes(y = sizeps, colour = "size PS")) + 
#   geom_line(aes(y = sizepdf, colour = "size PDF")) +
#   geom_line(aes(y = sizepng, colour = "size PNG")) + 
#   geom_line(aes(y = sizejpg, colour = "size JPG")) 
# print(plot)
# ggsave(file = "final plot.jpg")
  
# The diamonds dataset within ggplot2 contains 10 columns (price, carat,
# cut, color, etc.) for 53940 di↵erent diamonds. Type help(diamonds) for
# more information. Plot histograms for color, carat, and price, and comment
# on their shapes. Investigate the three-way relationship between price, carat,
# and cut. What are your conclusions? Provide graphs that support your
# conclusions. If you encounter computational di!culties, consider using a
# smaller dataframe whose rows are sampled from the original diamonds
# dataframe. Use the function sample to create a subset of indices that
# may be used to create the smaller dataframe.
# hist(diamonds$carat)
print(ggplot(data=diamonds, aes(diamonds$carat)) + 
        geom_histogram(binwidth=0.1,fill="white",colour="black")+
        geom_histogram(binwidth=0.05,fill="black",colour="black")+
        geom_histogram(binwidth=0.01,fill="red",colour="red"))

print(ggplot(data=diamonds, aes(diamonds$price)) + 
        geom_histogram(binwidth=0.1,fill="white",colour="black")+
        geom_histogram(binwidth=0.05,fill="black",colour="black")+
        geom_histogram(binwidth=0.01,fill="red",colour="red"))

# hist(diamonds$price)
# ggplot(data=diamonds, aes(as.numeric(diamonds$color))) + geom_histogram(binwidth=1,fill="white",colour="black")
# 
# plot10<-ggplot(diamonds, aes(x=carat, y=price, color=cut)) + geom_point() + geom_smooth(se=FALSE)
# print(plot10)
# 
# plot11<-ggplot(diamonds, aes(x=carat, y=price, color=cut)) + geom_smooth(se=FALSE)
# print(plot11)